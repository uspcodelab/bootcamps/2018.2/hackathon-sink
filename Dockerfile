FROM node:10.11.0-alpine

ENV APP_PATH=/usr/src/app

WORKDIR ${APP_PATH}

COPY package.json package-lock.json ./

RUN npm install

COPY index.js ./

CMD node index.js
